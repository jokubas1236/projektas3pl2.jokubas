﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.ViewModels;
using Library.CustomExeptions;
using MySql.Data.MySqlClient;

namespace Library.Services
{
    public class BookOrderService : DefaultService
    {
        public BookOrderService()
        {

        }

        public BookReaderViewModel SaveBookReader(BookReaderViewModel viewModel)
        {
            try
            {
                if (viewModel.Id.HasValue)
                {
                    //update
                    var bookReaderUpdateCmdText = "UPDATE bookreaders SET FirstName = @FirstName, LastName = @LastName, Phone = @Phone, Email = @Email WHERE Id = @Id";

                    var bookReaderUpdateCmd = new MySqlCommand(bookReaderUpdateCmdText, libraryConnection);
                    bookReaderUpdateCmd.Parameters.AddWithValue("@FirstName", viewModel.FirstName);
                    bookReaderUpdateCmd.Parameters.AddWithValue("@LastName", viewModel.LastName);
                    bookReaderUpdateCmd.Parameters.AddWithValue("@Phone", viewModel.Phone);
                    bookReaderUpdateCmd.Parameters.AddWithValue("@Email", viewModel.Email);
                    bookReaderUpdateCmd.Parameters.AddWithValue("@Id", viewModel.Id);
                    OpenConnection();
                    bookReaderUpdateCmd.ExecuteNonQuery();
                    CloseConnection();
                } else
                {
                    //insert
                    var bookReaderAddCmdText = "INSERT INTO bookreaders (FirstName, LastName, Phone, Email) VALUES (@FirstName, @LastName, @Phone, @Email);";

                    var bookReaderAddCmd = new MySqlCommand(bookReaderAddCmdText, libraryConnection);
                    bookReaderAddCmd.Parameters.AddWithValue("@FirstName", viewModel.FirstName);
                    bookReaderAddCmd.Parameters.AddWithValue("@LastName", viewModel.LastName);
                    bookReaderAddCmd.Parameters.AddWithValue("@Phone", viewModel.Phone);
                    bookReaderAddCmd.Parameters.AddWithValue("@Email", viewModel.Email);

                    OpenConnection();
                    bookReaderAddCmd.ExecuteNonQuery();
                    viewModel.Id = Convert.ToInt32(bookReaderAddCmd.LastInsertedId);
                    CloseConnection();
                }

                return viewModel;
            }
            catch (Exception e)
            {
                throw new FailedSaveBookReaderExeption(e);
            }
        }
    }
}
