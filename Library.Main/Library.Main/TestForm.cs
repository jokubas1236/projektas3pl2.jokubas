﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.ViewModels;

namespace Library.Main
{
	public partial class TestForm : BaseForm
	{
		public TESTViewModel tESTViewModel { get; set; }

		public List<TestClass> list = new List<TestClass>() {
			new TestClass() { TestProperty1 = 1, TestProperty2 = "a1"},
			new TestClass() { TestProperty1 = 2, TestProperty2 = "a2" },
			new TestClass() { TestProperty1 = 3, TestProperty2 = "a3" }
		};
		public TestForm(TESTViewModel tESTViewModel = null)
		{
			InitializeComponent();
			
			PrepareForm();
			FillForm(tESTViewModel);
			
		}

		private void PrepareForm()
		{
			cmbxEnum1.DataSource = Enum.GetValues(typeof(Enumerations.Enum1));
			cmbxEnum1.SelectedIndex = -1;
			listboxValue1.DataSource = new BindingSource(list, null);
			listboxValue1.ValueMember = "TestProperty1";
			listboxValue1.DisplayMember = "TestProperty2";

			cmbxList2.DataSource = new BindingSource(list, null);
			cmbxList2.ValueMember = "TestProperty1";
			cmbxList2.DisplayMember = "TestProperty2";

			listboxList1.DataSource = new BindingSource(list, null);
			listboxList1.DisplayMember = "TestProperty2";
			listboxList1.ValueMember = "TestProperty1";
		}

		private void cmbxList2_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
	}
}
