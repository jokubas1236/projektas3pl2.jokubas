﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.ViewModels;

namespace Library.Main
{
	public partial class Form2 : BaseForm
	{
		public List<TestClass> list = new List<TestClass>() {
			new TestClass() { TestProperty1 = 1, TestProperty2 = "a1"},
			new TestClass() { TestProperty1 = 2, TestProperty2 = "a2" },
			new TestClass() { TestProperty1 = 3, TestProperty2 = "a3" }
		};

		public Form2(TESTViewModel viewModel = null)
		{
			InitializeComponent();
			PrepareForm();
		}

		private void PrepareForm()
		{
		
		}
	}
}
