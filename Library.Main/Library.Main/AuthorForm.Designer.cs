﻿namespace Library.Main
{
    partial class AuthorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAuthorFirstname = new System.Windows.Forms.TextBox();
            this.txtAuthorLastname = new System.Windows.Forms.TextBox();
            this.btnSaveAuthor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAuthorId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtAuthorFirstname
            // 
            this.txtAuthorFirstname.Location = new System.Drawing.Point(66, 8);
            this.txtAuthorFirstname.Name = "txtAuthorFirstname";
            this.txtAuthorFirstname.Size = new System.Drawing.Size(100, 20);
            this.txtAuthorFirstname.TabIndex = 0;
            // 
            // txtAuthorLastname
            // 
            this.txtAuthorLastname.Location = new System.Drawing.Point(66, 31);
            this.txtAuthorLastname.Name = "txtAuthorLastname";
            this.txtAuthorLastname.Size = new System.Drawing.Size(100, 20);
            this.txtAuthorLastname.TabIndex = 1;
            // 
            // btnSaveAuthor
            // 
            this.btnSaveAuthor.Location = new System.Drawing.Point(66, 57);
            this.btnSaveAuthor.Name = "btnSaveAuthor";
            this.btnSaveAuthor.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAuthor.TabIndex = 2;
            this.btnSaveAuthor.Text = "button1";
            this.btnSaveAuthor.UseVisualStyleBackColor = true;
            this.btnSaveAuthor.Click += new System.EventHandler(this.btnSaveAuthor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Firstname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Lastname";
            // 
            // txtAuthorId
            // 
            this.txtAuthorId.Location = new System.Drawing.Point(224, 4);
            this.txtAuthorId.Name = "txtAuthorId";
            this.txtAuthorId.Size = new System.Drawing.Size(30, 20);
            this.txtAuthorId.TabIndex = 5;
            this.txtAuthorId.Visible = false;
            // 
            // AuthorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 165);
            this.Controls.Add(this.txtAuthorId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSaveAuthor);
            this.Controls.Add(this.txtAuthorLastname);
            this.Controls.Add(this.txtAuthorFirstname);
            this.Name = "AuthorForm";
            this.Text = "AuthorForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAuthorFirstname;
        private System.Windows.Forms.TextBox txtAuthorLastname;
        private System.Windows.Forms.Button btnSaveAuthor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAuthorId;
    }
}